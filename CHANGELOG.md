## 5.0.1

- fix: `var.project_approval_rules` was improperly validated

## 5.0.0

- feat: (BREAKING) removes `var.approvals_before_merge`, replaced by `var.project_approval_rules`.

## 4.1.1

- fix: pre-conditions were failing

## 4.1.0

- feat: allows to pass multiple `user_ids` or `group_ids` with `var.tag_protections`
- test: adjust gitlab-ci options
- chore: remove `moved`

## 4.0.0

- feat: adds `var.feature_flags_access_level`, `var.model_registry_access_level` and `var.model_experiments_access_level`, disabled by default.
- maintenance: (BREAKING) bumps provider requirement to 17+

## 3.1.1

- fix: resource `gitlab_label` deprecated in favor of `gitlab_project_label`

## 3.1.0

- feat: adds `var.deploy_keys` to add deploy keys to the project

## 3.0.0

- feat: (BREAKING) `var.tag_protection` (with default "*.*.*") object is now `var.tag_protections` map of objects (without defaults)
- feat: (BREAKING) replaces deprecated `var.public_builds` with `var.public_jobs`
- feat: (BREAKING) replaces deprecated `var.emails_disabled` (false) is now `var.emails_enabled` (true)
- feat: (BREAKING) replaces deprecated `var.container_expiration_policy.name_regex` by `var.container_expiration_policy.name_regex_delete`
- feat: adds new `var.monitor_access_level` and `var.environments_access_level` and `var.releases_access_level` and `var.infrastructure_access_level`
- maintenance: (BREAKING) requires provider version 16+
- maintenance: modernize project

## 2.0.2

- fix: wrong condition on `var.push_mirror`

## 2.0.1

- fix: for_each sensitive value with TF 1.6+
- doc: update changelog to comply with all other Wildbeavers repos
- chore: bump pre-commit hooks

## 2.0.0

- feat: (BREAKING) removes deprecated `operations_access_level`
- feat: (BREAKING) change deprecated `name_regex_delete` to `name_regex`

## 1.3.0

- feat: handles new `import_url_username` and `import_url_password`

## 1.2.0

- fix: fixes the user of custom template
- fix: makes `access_tokens` nullable
- feat: adds needed variables for templates `group_with_project_templates_id`, `template_name` and `use_custom_template`

## 1.1.0

- feat: splits `access_tokens` outputs in sub-parts, because of the `sensitive`
- test: uses reusable `terraform-module` pipeline.

## 1.0.0

- feat: first version
