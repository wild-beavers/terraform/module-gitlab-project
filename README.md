# terraform-module-gitlab-project

Generic module to manage gitlab projects and associated resources.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.3.0 |
| gitlab | ~> 17.0 |

## Providers

| Name | Version |
|------|---------|
| gitlab | ~> 17.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_branch_protection.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch_protection) | resource |
| [gitlab_deploy_key.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/deploy_key) | resource |
| [gitlab_project.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project) | resource |
| [gitlab_project_access_token.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_access_token) | resource |
| [gitlab_project_approval_rule.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_approval_rule) | resource |
| [gitlab_project_badge.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_badge) | resource |
| [gitlab_project_hook.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_hook) | resource |
| [gitlab_project_label.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_label) | resource |
| [gitlab_project_level_mr_approvals.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_level_mr_approvals) | resource |
| [gitlab_project_mirror.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_mirror) | resource |
| [gitlab_project_variable.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_tag_protection.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/tag_protection) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| access\_tokens | Allows to manage the lifecycle of a project access token.<br/>keys are the name to describe the project access token.<br/><br/>  scopes        (set(string), required) Valid values: `api`, `read_api`, `read_repository`, `write_repository`, `read_registry`, `write_registry`.<br/>  access\_level  (string, optional)      The access level for the project access token. Valid values are: `no one`, `minimal`, `guest`, `reporter`, `developer`,<br/>  expires\_at    (string, optional)      Time the token will expire it, `YYYY-MM-DD` format. Will not expire per default. | <pre>map(object({<br/>    scopes       = set(string)<br/>    access_level = optional(string)<br/>    expires_at   = optional(string)<br/>  }))</pre> | `null` | no |
| allow\_merge\_on\_skipped\_pipeline | Set to `true` if you want to treat skipped pipelines as if they finished with success. | `bool` | `false` | no |
| analytics\_access\_level | Set the analytics access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| archive\_on\_destroy | Set to `true` to archive the project instead of deleting on destroy. If set to `true` it will entire omit the `DELETE` operation. | `bool` | `true` | no |
| archived | Whether the project is in read-only mode (archived). Repositories can be archived/unarchived by toggling this parameter. | `bool` | `false` | no |
| auto\_cancel\_pending\_pipelines | Auto-cancel pending pipelines. This isn’t a boolean, but `enabled`/`disabled`. | `string` | `"enabled"` | no |
| auto\_devops\_deploy\_strategy | Auto Deploy strategy. Valid values are `continuous`, `manual`, `timed_incremental`. | `string` | `null` | no |
| auto\_devops\_enabled | Enable Auto DevOps for this project. | `bool` | `false` | no |
| autoclose\_referenced\_issues | Set whether auto-closing referenced issues on default branch. | `bool` | `true` | no |
| avatar | A local path to the avatar image to upload. Note: not available for imported resources. | `string` | `null` | no |
| badges | Allows to manage the lifecycle of group badges.<br/>Key is free and might be anything<br/><br/>  link\_url   (string, required) The URL linked with the badge.<br/>  image\_url  (string, required) The image URL which will be presented on group overview. | <pre>map(object({<br/>    link_url  = string<br/>    image_url = string<br/>  }))</pre> | `null` | no |
| badges\_use\_default | Adds default badge to the list of badges, the gitlab pipeline badge for the current project. This used index `999` of `var.badges`. | `bool` | `true` | no |
| branch\_protection\_allow\_force\_push | n/a | `bool` | `true` | no |
| branch\_protection\_allowed\_to\_merge | n/a | <pre>list(object({<br/>    user_id = number<br/>  }))</pre> | `[]` | no |
| branch\_protection\_allowed\_to\_push | n/a | <pre>list(object({<br/>    user_id = number<br/>  }))</pre> | `[]` | no |
| branch\_protection\_branches | n/a | `list(string)` | `[]` | no |
| branch\_protection\_code\_owner\_approval\_required | n/a | `bool` | `false` | no |
| branch\_protection\_merge\_access\_level | n/a | `string` | `"maintainer"` | no |
| branch\_protection\_push\_access\_level | n/a | `string` | `"no one"` | no |
| build\_git\_strategy | The Git strategy. Defaults to `fetch`. | `string` | `null` | no |
| build\_timeout | The maximum amount of time, in seconds, that a job can run. | `number` | `null` | no |
| builds\_access\_level | Set the builds access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"private"` | no |
| ci\_config\_path | Custom Path to CI config file. | `string` | `null` | no |
| ci\_default\_git\_depth | Default number of revisions for shallow cloning. | `number` | `null` | no |
| ci\_forward\_deployment\_enabled | When a new deployment job starts, skip older deployment jobs that are still pending. | `bool` | `false` | no |
| ci\_separated\_caches | Use separate caches for protected branches. | `bool` | `true` | no |
| container\_expiration\_policy | Set the image cleanup policy for this project. Ignored if var.container\_registry\_access\_level is "disabled"<br/><br/>    cadence           (string, optional) The cadence of the policy. Valid values are: `1d`, `7d`, `14d`, `1month`, `3month`.<br/>    enabled           (bool, optional)   If true, the policy is enabled.<br/>    keep\_n            (number, optional) The number of images to keep.<br/>    name\_regex\_delete        (string, optional) The regular expression to match image names to delete.<br/>    name\_regex\_keep   (number, optional) The regular expression to match image names to keep.<br/>    older\_than        (string, optional) The number of days to keep images. | <pre>object({<br/>    cadence         = optional(string, "1d")<br/>    enabled         = optional(bool, true)<br/>    keep_n          = optional(number)<br/>    name_regex      = optional(string, ".*")<br/>    name_regex_keep = optional(string, "^([0-9]+(\\.[0-9]+){0,2})$")<br/>    older_than      = optional(string, "14d")<br/>  })</pre> | `{}` | no |
| container\_registry\_access\_level | Set visibility of container registry, for this project. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| default\_branch | The default branch for the project. | `string` | `"master"` | no |
| deploy\_keys | Manage deployment SSH keys.<br/>Keys (of the map) are free values.<br/><br/>  key      (string, required) The public ssh key body.<br/>  title    (string, required) A title to describe the deploy key with.<br/>  can\_push (bool, optional, false) Allow this deploy key to be used to push changes to the project. | <pre>map(object({<br/>    key      = string<br/>    title    = string<br/>    can_push = optional(bool, false)<br/>  }))</pre> | `{}` | no |
| description | n/a | `string` | n/a | yes |
| emails\_enabled | Enable email notifications. | `bool` | `true` | no |
| environments\_access\_level | Set the environments access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| external\_authorization\_classification\_label | The classification label for the project. | `string` | `null` | no |
| external\_project\_id | ID of an existing project to update. | `string` | `null` | no |
| feature\_flags\_access\_level | Set the feature flags access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| forked\_from\_project\_id | Id of the project to fork. During create the project is forked and during an update the fork relation is changed. | `number` | `null` | no |
| forking\_access\_level | Set the forking access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"enabled"` | no |
| group\_with\_project\_templates\_id | For group-level custom templates, specifies ID of group from which all the custom project templates are sourced. Leave empty for instance-level templates. Requires `use_custom_template` to be true (enterprise edition). | `number` | `null` | no |
| hooks | Allows to manage the lifecycle of a project hook.<br/><br/>  url                        (string, required) The url of the hook to invoke.<br/>  confidential\_issues\_events (bool, optional)   Invoke the hook for confidential issues events.<br/>  confidential\_note\_events   (bool, optional)   Invoke the hook for confidential notes events.<br/>  deployment\_events          (bool, optional)   Invoke the hook for deployment events.<br/>  enable\_ssl\_verification    (bool, optional)   Enable ssl verification when invoking the hook.<br/>  issues\_events              (bool, optional)   Invoke the hook for issues events.<br/>  job\_events                 (bool, optional)   Invoke the hook for job events.<br/>  merge\_requests\_events      (bool, optional)   Invoke the hook for merge requests.<br/>  note\_events                (bool, optional)   Invoke the hook for notes events.<br/>  pipeline\_events            (bool, optional)   Invoke the hook for pipeline events.<br/>  push\_events                (bool, optional)   Invoke the hook for push events.<br/>  push\_events\_branch\_filter  (string, optional) Invoke the hook for push events on matching branches only.<br/>  releases\_events            (bool, optional)   Invoke the hook for releases events.<br/>  tag\_push\_events            (bool, optional)   Invoke the hook for tag push events.<br/>  token                      (string, optional) A token to present when invoking the hook. The token is not available for imported resources.<br/>  wiki\_page\_events           (bool, optional)   Invoke the hook for wiki page events. | <pre>set(object({<br/>    url                        = string<br/>    confidential_issues_events = optional(bool)<br/>    confidential_note_events   = optional(bool)<br/>    deployment_events          = optional(bool)<br/>    enable_ssl_verification    = optional(bool, true)<br/>    issues_events              = optional(bool)<br/>    job_events                 = optional(bool)<br/>    merge_requests_events      = optional(bool)<br/>    note_events                = optional(bool)<br/>    pipeline_events            = optional(bool)<br/>    push_events                = optional(bool)<br/>    push_events_branch_filter  = optional(string)<br/>    releases_events            = optional(bool)<br/>    tag_push_events            = optional(bool)<br/>    token                      = optional(string)<br/>    wiki_page_events           = optional(bool)<br/>  }))</pre> | `[]` | no |
| import\_url | Git URL to a repository to be imported. Together with `mirror = true` it will setup a Pull Mirror. This can also be used together with `forked_from_project_id` to setup a Pull Mirror for a fork. The fork takes precedence over the import. This field cannot be imported via `terraform import`. | `string` | `null` | no |
| import\_url\_password | The password for the `import_url`. The value of this field is used to construct a valid `import_url` and is only related to the provider. This field cannot be imported using terraform import. See the examples section for how to properly use it. | `string` | `null` | no |
| import\_url\_username | The username for the `import_url`. The value of this field is used to construct a valid `import_url` and is only related to the provider. This field cannot be imported using terraform import. See the examples section for how to properly use it. | `string` | `null` | no |
| infrastructure\_access\_level | Set the infrastructure access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| initialize\_with\_readme | Create main branch with first commit containing a README.md file. | `bool` | `false` | no |
| issues\_access\_level | Set the issues access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| issues\_enabled | Enable issue tracking for the project. | `bool` | `true` | no |
| issues\_template | Sets the template for new issues in the project. | `string` | `null` | no |
| labels | Allows to manage the lifecycle of a project label.<br/>Key are the name of the label.<br/><br/>  color       (string, required) Color of the label given in 6-digit hex notation with leading '#' sign (e.g. `#FFAABB`) or one of the [CSS color names](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#Color_keywords).<br/>  description (string, optional) Description of the label. | <pre>map(object({<br/>    color       = string<br/>    description = optional(string)<br/>  }))</pre> | `null` | no |
| level\_mr\_approvals | allows to manage the lifecycle of a Merge Request-level approval rule.<br/><br/>    disable\_overriding\_approvers\_per\_merge\_request (bool, optional) By default, users are able to edit the approval rules in merge requests.<br/>    merge\_requests\_author\_approval                 (bool, optional) Set to `true` if you want to allow merge request authors to self-approve merge requests.<br/>    merge\_requests\_disable\_committers\_approval     (bool, optional) Set to `true` if you want to prevent approval of merge requests by merge request committers.<br/>    require\_password\_to\_approve                    (bool, optional) Set to `true` if you want to require authentication when approving a merge request.<br/>    reset\_approvals\_on\_push                        (bool, optional) Set to `true` if you want to remove all approvals in a merge request when new commits are pushed to its source branch. Default is `true`. | <pre>object({<br/>    disable_overriding_approvers_per_merge_request = optional(bool, true)<br/>    merge_requests_author_approval                 = optional(bool, true)<br/>    merge_requests_disable_committers_approval     = optional(bool, false)<br/>    require_password_to_approve                    = optional(bool, false)<br/>    reset_approvals_on_push                        = optional(bool, false)<br/>  })</pre> | `{}` | no |
| lfs\_enabled | Enable LFS for the project. | `bool` | `false` | no |
| merge\_commit\_template | Template used to create merge commit message in merge requests. (Introduced in GitLab 14.5.) | `string` | `null` | no |
| merge\_method | Set the merge method. Valid values are `merge`, `rebase_merge`, `ff`. | `string` | `"ff"` | no |
| merge\_pipelines\_enabled | Enable or disable merge pipelines. | `bool` | `false` | no |
| merge\_requests\_access\_level | Set the merge requests access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"enabled"` | no |
| merge\_requests\_enabled | Enable merge requests for the project. | `bool` | `true` | no |
| merge\_requests\_template | Sets the template for new merge requests in the project. | `string` | `null` | no |
| merge\_trains\_enabled | Enable or disable merge trains. Requires `merge_pipelines_enabled` to be set to `true` to take effect. | `bool` | `false` | no |
| mirror | Enable project pull mirror. | `bool` | `false` | no |
| mirror\_overwrites\_diverged\_branches | Enable overwrite diverged branches for a mirrored project. | `bool` | `false` | no |
| mirror\_trigger\_builds | Enable trigger builds on pushes for a mirrored project. | `bool` | `true` | no |
| model\_experiments\_access\_level | Set visibility of machine learning model experiments. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| model\_registry\_access\_level | Set visibility of machine learning model registry. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| monitor\_access\_level | Set the monitor access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| name | Name of the project. | `string` | n/a | yes |
| namespace\_id | The namespace (group or user) of the project. Defaults to your user. | `number` | n/a | yes |
| only\_allow\_merge\_if\_all\_discussions\_are\_resolved | Set to `true` if you want allow merges only if all discussions are resolved. | `bool` | `true` | no |
| only\_allow\_merge\_if\_pipeline\_succeeds | Set to `true` if you want allow merges only if a pipeline succeeds. | `bool` | `true` | no |
| only\_mirror\_protected\_branches | Enable only mirror protected branches for a mirrored project. | `bool` | `true` | no |
| packages\_enabled | Enable packages repository for the project. | `bool` | `false` | no |
| pages\_access\_level | Enable pages access control | `string` | `"disabled"` | no |
| path | Path of the repository. | `string` | `null` | no |
| printing\_merge\_request\_link\_enabled | Show link to create/view merge request when pushing from the command line | `bool` | `true` | no |
| project\_approval\_rules | Allows to manage the lifecycle of a project-level approval rule.<br/>Keys are free values.<br/><br/>  - name               (string, required): Name of the approval rule.<br/>  - approvals\_required (number, required): Number of approvals required for this rule.<br/><br/>  - applies\_to\_all\_protected\_branches                     optional(bool):        Whether the rule is applied to all protected branches. If set to 'true', the value of protected\_branch\_ids is ignored.<br/>  - disable\_importing\_default\_any\_approver\_rule\_on\_create optional(bool):        When this flag is set, the default any\_approver rule will not be imported if present.<br/>  - group\_ids                                             optional(set(number)): Group IDs whose members can approve of the merge request.<br/>  - protected\_branch\_ids                                  optional(set(number)): List of protected branch IDs (not branch names) for which the rule applies.<br/>  - report\_type                                           optional(string):      Report type is required when the rule\_type is `report_approver`. Valid values are `code_coverage`.<br/>  - rule\_type                                             optional(string):      The type of rule. `any_approver` is a pre-configured default rule with `approvals_required` at `0`. Valid values are `regular`, `any_approver`, `report_approver`.<br/>  - user\_ids                                              optional(set(number)): List of specific User IDs to add to the list of approvers. | <pre>map(object({<br/>    name                                                  = string<br/>    approvals_required                                    = number<br/>    applies_to_all_protected_branches                     = optional(bool, false)<br/>    disable_importing_default_any_approver_rule_on_create = optional(bool, false)<br/>    group_ids                                             = optional(set(number))<br/>    protected_branch_ids                                  = optional(set(number))<br/>    report_type                                           = optional(string)<br/>    rule_type                                             = optional(string)<br/>    user_ids                                              = optional(set(number))<br/>  }))</pre> | `null` | no |
| public\_jobs | If true, jobs can be viewed by non-project members. | `bool` | `false` | no |
| push\_mirror | Allows to manage the lifecycle of a project mirror.<br/>This is for pushing changes to a remote repository. Pull Mirroring can be configured using a combination of the import\_url, mirror, and mirror\_trigger\_builds properties on the gitlab\_project resource.<br/><br/>  url                     (string, sensitive, required) The URL of the remote repository to be mirrored.<br/>  enabled                 (bool, optional)              Determines if the mirror is enabled.<br/>  keep\_divergent\_refs     (bool, optional)              Determines if divergent refs are skipped.<br/>  only\_protected\_branches (bool, optional)              Determines if only protected branches are mirrored. | <pre>object({<br/>    url                     = string<br/>    enabled                 = optional(bool, true)<br/>    keep_divergent_refs     = optional(bool, true)<br/>    only_protected_branches = optional(bool, true)<br/>  })</pre> | `null` | no |
| push\_rules | Push rules for the project.<br/><br/>  author\_email\_regex            (string, optional) All commit author emails must match this regex, e.g. @my-company.com$.<br/>  branch\_name\_regex             (string, optional) All branch names must match this regex, e.g. (feature\|hotfix)\/*.<br/>  commit\_committer\_check        (bool, optional)   Users can only push commits to this repository that were committed with one of their own verified emails.<br/>  commit\_message\_negative\_regex (string, optional) No commit message is allowed to match this regex, for example ssh\:\/\/.<br/>  commit\_message\_regex          (string, optional) All commit messages must match this regex, e.g. Fixed \d+\..*.<br/>  deny\_delete\_tag               (bool, optional)   Deny deleting a tag.<br/>  file\_name\_regex               (string, optional) All commited filenames must not match this regex, e.g. (jar\|exe)$.<br/>  max\_file\_size                 (number, optional) Maximum file size (MB).<br/>  member\_check                  (bool, optional)   Restrict commits by author (email) to existing GitLab users.<br/>  prevent\_secrets               (bool, optional)   GitLab will reject any files that are likely to contain secrets.<br/>  reject\_unsigned\_commits       (bool, optional)   Reject commit when it’s not signed through GPG. | <pre>object({<br/>    author_email_regex            = optional(string, )<br/>    branch_name_regex             = optional(string)<br/>    commit_committer_check        = optional(bool, true)<br/>    commit_message_negative_regex = optional(string)<br/>    commit_message_regex          = optional(string, "(feat|fix|chore|tech|refactor|doc|maintenance|test|revert): ")<br/>    deny_delete_tag               = optional(bool)<br/>    file_name_regex               = optional(string, "(exe|tar|tar.gz|tar.xz|zip|7zip|rar|bin|jar|doc|docx|xlsx|xls)$")<br/>    max_file_size                 = optional(number, 2)<br/>    member_check                  = optional(bool)<br/>    prevent_secrets               = optional(bool, true)<br/>    reject_unsigned_commits       = optional(bool, true)<br/>  })</pre> | `{}` | no |
| releases\_access\_level | Set the releases access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| remove\_source\_branch\_after\_merge | Enable `Delete source branch` option by default for all new merge requests. | `bool` | `true` | no |
| repository\_access\_level | Set the repository access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"enabled"` | no |
| request\_access\_enabled | Allow users to request member access. | `bool` | `true` | no |
| requirements\_access\_level | Set the requirements access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| resolve\_outdated\_diff\_discussions | Automatically resolve merge request diffs discussions on lines changed with a push. | `bool` | `true` | no |
| restrict\_user\_defined\_variables | Allow only users with the Maintainer role to pass user-defined variables when triggering a pipeline. | `bool` | `true` | no |
| security\_and\_compliance\_access\_level | Set the security and compliance access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"private"` | no |
| shared\_runners\_enabled | Enable shared runners for this project. | `bool` | `null` | no |
| snippets\_access\_level | Set the snippets access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| snippets\_enabled | Enable snippets for the project. | `bool` | `false` | no |
| squash\_commit\_template | Template used to create squash commit message in merge requests. | `string` | `null` | no |
| squash\_option | Squash commits when merge request. Valid values are `never`, `always`, `default_on`, or `default_off`. The default value is `default_off`. | `string` | `null` | no |
| suggestion\_commit\_message | The commit message used to apply merge request suggestions. | `string` | `null` | no |
| tag\_protections | Manage tags protections.<br/>Keys are free values.<br/>    tag                 (string, required)       Name of the tag or wildcard.<br/>    create\_access\_level (string, required)       Access levels which are allowed to create. Valid values are: `no one`, `developer`, `maintainer`.<br/>    user\_id             (number, optional)       The ID of a GitLab user allowed to perform the relevant action. Mutually exclusive with group\_id.<br/>    group\_id            (number, optional)       The ID of a GitLab group allowed to perform the relevant action. Mutually exclusive with `user_id`.<br/>    user\_ids            (list(number), optional) IDs of GitLab users allowed to perform the relevant action. Mutually exclusive with `group_ids`.<br/>    group\_ids           (list(number), optional) IDs of a GitLab group allowed to perform the relevant action. Mutually exclusive with `user_ids`. | <pre>map(object({<br/>    tag                 = string<br/>    create_access_level = string<br/>    user_id             = optional(number)<br/>    group_id            = optional(number)<br/>    user_ids            = optional(list(number), [])<br/>    group_ids           = optional(list(number), [])<br/>  }))</pre> | `{}` | no |
| template\_name | When used without use\_custom\_template, name of a built-in project template. When used with use\_custom\_template, name of a custom project template. This option is mutually exclusive with `template_project_id`. | `string` | `null` | no |
| template\_project\_id | When used with `use_custom_template`, project ID of a custom project template. This is preferable to using `template_name` since `template_name` may be ambiguous (enterprise edition). This option is mutually exclusive with `template_name`. See `gitlab_group_project_file_template` to set a project as a template project. If a project has not been set as a template, using it here will result in an error. | `string` | `null` | no |
| topics | The list of topics for a project | `set(string)` | `[]` | no |
| use\_custom\_template | Use either custom instance or group (with group\_with\_project\_templates\_id) project template (enterprise edition). | `bool` | `false` | no |
| variables | The gitlab\_project\_variable resource allows to manage the lifecycle of a CI/CD variable for a project.<br/>keys are the name of the variables.<br/><br/>  value         (string, required) Value of the variable.<br/>  protected     (bool, optional)   If set to `true`, the variable will be passed only to pipelines running on protected branches and tags. Defaults to `false`.<br/>  masked        (bool, optional)   If set to `true`, the value of the variable will be hidden in job logs. The value must meet the masking requirements. Defaults to `false`.<br/>  variable\_type (string, optional) The type of a variable. Valid values are: `env_var`, `file`. Default is `env_var`. | <pre>map(object({<br/>    value         = string<br/>    protected     = optional(bool)<br/>    masked        = optional(bool)<br/>    variable_type = optional(string)<br/>  }))</pre> | `{}` | no |
| visibility\_level | Set to `public` to create a public project. | `string` | `"public"` | no |
| wiki\_access\_level | Set the wiki access level. Valid values are `disabled`, `private`, `enabled`. | `string` | `"disabled"` | no |
| wiki\_enabled | Enable wiki for the project. | `bool` | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| access\_tokens | n/a |
| access\_tokens\_active | n/a |
| access\_tokens\_created\_at | n/a |
| access\_tokens\_ids | n/a |
| access\_tokens\_revoked | n/a |
| access\_tokens\_user\_ids | n/a |
| access\_tokens\_user\_tokens | n/a |
| avatar\_url | n/a |
| badges | n/a |
| deploy\_key | n/a |
| http\_url\_to\_repo | n/a |
| id | n/a |
| path\_with\_namespace | n/a |
| runners\_token | n/a |
| ssh\_url\_to\_repo | n/a |
| web\_url | n/a |
<!-- END_TF_DOCS -->
